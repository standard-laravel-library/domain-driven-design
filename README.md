# Service Provider
The `ServiceProvider` offers a minimal & unified set of configuration driven utilities that allows convenient & consistent way to interact with the Laravel Container and key components of the framework.

# Version Compatibility
 Laravel  |  Service Provider
:----------|:----------
 10.x      | 1.x
 11.x      | 2.x
 12.x      | 3.x

# Getting started
Extend the provided `ServiceProvider` in your application or package:
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
```

# Features
## Interacts with Console
Similar to `App\Console\Kernel`, commands defined in the `$commands` will be automatically registered to the application.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $commands = [
        \Gitlab\Console\Commands\CloseStaleIssues::class
    ];
}
```

## Interacts with Events
As with `App\Providers\EventServiceProvider` listeners and subscribers are easily registered.

```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $listen = [
        \Gitlab\Issues\Events\Opened::class => [
            \Gitlab\Issues\Listeners\NotifyAdministrators::class
        ]
    ];

    protected array $subscribe = [
        \Gitlab\Issues\Events\Subscribers\IssueSubscriber::class
    ];
}
```

Additionally, `Model` observers can also be registered.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $observers = [
        \Gitlab\Issues\Models\Issue::class => [
            \Gitlab\Issues\Observers\Issue::class
        ]
    ];
}
```

## Interacts with Gate
Convenient policy registration is available similar to `App\Providers\AuthServiceProvider`.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $policies = [
        \Gitlab\Issues\Models\Issue::class => \Gitlab\Issues\Policies\Issue::class
    ];
}
```

Additionally, `Gate` definitions can be specified.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function bootGateDefinitions(): void
    {
        Gate::define(
            'manage',
            fn () => auth()->user()->isAdmin()
        );
    }
}
```

## Interacts with Relaions
Polymorphic class aliases can be defined.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $morphMap = [
        'repository' => \Gitlab\Repositories\Models\Repository::class
    ];
}
```

Additionally, a convenience is available to enforce morph aliases for each `Model`.
```php
namespace App\Providers;

class AppServiceProviders extends \Standard\Library\Providers\ServiceProvider
{
    protected bool $requireMorphMap = true;
}
```

> Note: Enabling enforcement only needs to happen once. Should you wish to enable this feature we recommend setting this flag on your `AppServiceProvider`.

## Interacts with View
Views can be included via configuration with a path and namespace.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $views = [
        __DIR__ . '/../resources/views' => 'gitlab'
    ];
}
```

Or programmatically:
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function views(): iterable
    {
        return Storage::directories(__DIR__ . '/..resources/views')->mapWithKeys(
            fn ($directory) => [Str::of('gitlab-')->append($directory) => $directory]
        );
    }
}
```
## Registers Providers
To assist with organization and dependencies (especially for Domain Driven Design), it can be useful to register other providers.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $providers = [
        \Gitlab\Issues\Providers\ServiceProvider::class,
        \Gitlab\MergeRequests\Providers\ServiceProvider::class,
    ];
}
```

## Registers Configuration
Config values can be loaded via configuration.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $config = [
        'repository' => [
            'branches' => [
                'defaults' => [
                    'branch' => 'main',
                    'template' => '%{id}'
                ]
            ]
        ]
    ];
}
```

Or programmatically,

```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function config(): array
    {
        return [
            'deployments' => [
                'tokens' => env('DEPLOYMENT_TOKEN')
            ]
        ];
    }
}
```

> Note, config is merged sequentially with each registered `\Standard\Library\Providers\ServiceProvider`. Therefore any value can be overwritten by providers pending registration.

## Interacts Scheduler
Scheduling tasks is also simple.
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function bootScheduledTasks(Schedule $schedule): void
    {
        $schedule->command(
            \Gitlab\Console\Commands\CloseStaleIssues::class
        )->daily();
    }
}
```

## Additional use cases
Beyond the provided conveniences, it is still possible to interact with the `registration` and `boot` processes of a typical `ServiceProvider`.

### Before automated register tasks
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function beforeRegister(): void
    {
        app()->bind(
            \Gitlab\Manager::class,
            fn () => new \Gitlab\Manager()
        );
    }
}
```

### After automated register tasks
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function afterRegister(): void
    {
        app()->bind(
            \Gitlab\Manager::class,
            fn () => new \Gitlab\Manager()
        );
    }
}
```

### Before automated boot tasks
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function beforeBoot(): void
    {
        View::share('gitlab', resolve(\Gitlab\Manager::class));
    }
}
```

### After automated boot tasks
```php
namespace Gitlab;

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected function afterBoot(): void
    {
        View::share('gitlab', resolve(\Gitlab\Manager::class));
    }
}
```
