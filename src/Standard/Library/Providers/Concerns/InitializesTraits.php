<?php

namespace Standard\Library\Providers\Concerns;

trait InitializesTraits
{
    protected array $traitInitializers = [];

    final protected function initializeTraits(): void
    {
        collect(class_uses_recursive(static::class))->each(function ($trait): void {
            $this->addTraitInitializer('register', $trait);
            $this->addTraitInitializer('boot', $trait);
        });
    }

    final protected function addTraitInitializer(string $prefix, string $trait): void
    {
        $method = $prefix . class_basename($trait);

        if (method_exists(static::class, $method)) {
            $this->traitInitializers[$prefix][] = $method;
        }
    }

    final protected function registerInitializers(): void
    {
        collect($this->traitInitializers['register'] ?? [])->each(
            fn ($method) => $this->$method()
        );
    }

    final protected function bootInitializers(): void
    {
        collect($this->traitInitializers['boot'] ?? [])->each(
            fn ($method) => $this->$method()
        );
    }
}
