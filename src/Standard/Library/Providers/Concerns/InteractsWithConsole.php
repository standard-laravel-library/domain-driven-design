<?php

namespace Standard\Library\Providers\Concerns;

trait InteractsWithConsole
{
    protected array $commands = [];

    final protected function registerInteractsWithConsole(): void
    {
        $this->registerCommands();
    }

    final protected function registerCommands(): void
    {
        $this->commands($this->commands);
    }
}
