<?php

namespace Standard\Library\Providers\Concerns;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;

trait InteractsWithEvents
{
    protected array $listen = [];

    protected array $subscribe = [];

    protected array $observers = [];

    final protected function bootInteractsWithEvents(): void
    {
        $this->bootEvents();
    }

    final protected function bootEvents(): void
    {
        $this->bootObservers();
        $this->bootListeners();
        $this->bootSubscribers();
    }

    final protected function bootListeners(): void
    {
        collect($this->listen)->each(
            fn ($listeners, $event) => collect(Arr::wrap($listeners))->each(
                fn ($listener) => Event::listen($event, $listener)
            )
        );
    }

    final protected function bootSubscribers(): void
    {
        collect($this->subscribe)->each(
            fn ($subscriber) => Event::subscribe($subscriber)
        );
    }

    final protected function bootObservers(): void
    {
        collect($this->observers)->each(
            fn ($observers, $model) => $model::observe(
                Arr::wrap($observers)
            )
        );
    }
}
