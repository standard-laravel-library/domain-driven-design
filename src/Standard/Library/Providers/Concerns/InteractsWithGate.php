<?php

namespace Standard\Library\Providers\Concerns;

use Illuminate\Support\Facades\Gate;

trait InteractsWithGate
{
    protected array $policies = [];

    final protected function bootInteractsWithGate(): void
    {
        $this->bootGate();
    }

    final protected function bootGate(): void
    {
        $this->bootPolicies();
        $this->bootGateDefinitions();
    }

    final protected function bootPolicies(): void
    {
        collect($this->policies)->each(
            fn ($policy, $class) => Gate::policy($class, $policy)
        );
    }

    protected function bootGateDefinitions(): void
    {
    }
}
