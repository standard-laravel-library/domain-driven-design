<?php

namespace Standard\Library\Providers\Concerns;

use Illuminate\Database\Eloquent\Relations\Relation;

trait InteractsWithRelations
{
    protected bool $requireMorphMap = false;

    protected array $morphMap = [];

    final protected function bootInteractsWithRelations(): void
    {
        $this->bootRequireMorphMap();
        $this->bootMorphMap();
    }

    final protected function bootRequireMorphMap(): void
    {
        if (! $this->requireMorphMap) {
            return;
        }

        Relation::requireMorphMap();
    }

    final protected function bootMorphMap(): void
    {
        Relation::morphMap(
            $this->morphMap
        );
    }
}
