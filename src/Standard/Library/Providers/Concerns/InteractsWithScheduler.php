<?php

namespace Standard\Library\Providers\Concerns;

use Illuminate\Console\Scheduling\Schedule;

trait InteractsWithScheduler
{
    final protected function bootInteractsWithScheduler(): void
    {
        $this->bootSchedule();
    }

    protected function bootScheduledTasks(Schedule $schedule): void
    {
    }

    final protected function bootSchedule(): void
    {
        app()->booted(
            fn () => $this->bootScheduledTasks(
                resolve(Schedule::class)
            )
        );
    }
}
