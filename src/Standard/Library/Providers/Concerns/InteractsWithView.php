<?php

namespace Standard\Library\Providers\Concerns;

trait InteractsWithView
{
    protected array $views = [];

    final protected function bootInteractsWithView(): void
    {
        $this->bootViews();
    }

    final protected function bootViews(): void
    {
        collect($this->views())->each(
            fn ($directory, $namespace) => $this->loadViewsFrom($directory, $namespace)
        );
    }

    protected function views(): iterable
    {
        return $this->views;
    }
}
