<?php

namespace Standard\Library\Providers\Concerns;

trait PublishesAssets
{
    final protected function bootPublishesAssets(): void
    {
        $this->bootPublishables();
    }

    final protected function bootPublishables(): void
    {
        collect($this->publishables())->each(
            fn ($paths, $group) => $this->publishes($paths, $group)
        );
    }

    protected function publishables(): iterable
    {
        return [];
    }
}
