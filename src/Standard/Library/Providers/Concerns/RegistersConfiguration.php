<?php

namespace Standard\Library\Providers\Concerns;

use Illuminate\Support\Arr;

trait RegistersConfiguration
{
    protected array $config = [];

    protected function config(): array
    {
        return $this->config;
    }

    final protected function registerConfiguration(): void
    {
        if (app()->configurationIsCached()) {
            return;
        }

        collect(Arr::dot($this->config()))->each(
            fn ($value, $key) => config()->set($key, $value)
        );
    }
}
