<?php

namespace Standard\Library\Providers\Concerns;

trait RegistersProviders
{
    protected array $providers = [];

    final protected function registerProviders(): void
    {
        collect($this->providers)->each(
            fn ($provider) => app()->register($provider)
        );
    }
}
