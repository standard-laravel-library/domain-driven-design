<?php

namespace Standard\Library\Providers;

abstract class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    use Concerns\InitializesTraits;
    use Concerns\InteractsWithConsole;
    use Concerns\InteractsWithEvents;
    use Concerns\InteractsWithGate;
    use Concerns\InteractsWithScheduler;
    use Concerns\InteractsWithRelations;
    use Concerns\PublishesAssets;
    use Concerns\InteractsWithView;
    use Concerns\RegistersProviders;
    use Concerns\RegistersConfiguration;

    public function __construct($app)
    {
        parent::__construct($app);
        $this->initializeTraits();
    }

    final public function register(): void
    {
        $this->registerConfiguration();
        $this->beforeRegister();
        $this->registerInitializers();
        $this->afterRegister();
        $this->registerProviders();
    }

    final public function boot(): void
    {
        $this->beforeBoot();
        $this->bootInitializers();
        $this->afterBoot();
    }

    protected function beforeRegister(): void
    {
    }

    protected function afterRegister(): void
    {
    }

    protected function beforeBoot(): void
    {
    }

    protected function afterBoot(): void
    {
    }
}
