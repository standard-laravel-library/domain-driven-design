<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Support\Collection;

trait InitializesTraitsCases
{
    public function test_it_initializes_register_traits()
    {
        $this->assertSame(
            'registerInitializer',
            resolve('registerInitializer')
        );
    }

    public function test_it_initializes_boot_traits()
    {
        $this->assertSame(
            'bootInitializer',
            collect()->bootInitializer()
        );
    }
}

trait Initializer
{
    protected function registerInitializer()
    {
        app()->bind('registerInitializer', fn () => 'registerInitializer');
    }

    protected function bootInitializer()
    {
        Collection::macro('bootInitializer', fn () => 'bootInitializer');
    }
}
