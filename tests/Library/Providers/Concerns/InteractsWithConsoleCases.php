<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Console;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Attribute\AsCommand;

trait InteractsWithConsoleCases
{
    public function test_it_registers_console_commands()
    {
        $this->assertArrayHasKey('command', Artisan::all());
    }
}

#[AsCommand(name: 'command')]
class Command extends Console\Command
{
}
