<?php

namespace Tests\Library\Providers\Concerns;

use ReflectionClass;
use Illuminate\Database\Eloquent;

trait InteractsWithEventsCases
{
    public function test_it_boots_model_observers()
    {
        $events = resolve('events');

        $this->assertArrayHasKey(
            'eloquent.creating: Tests\Library\Providers\Concerns\Model',
            (new ReflectionClass($events))->getProperty('listeners')->getValue($events)
        );
    }

    public function test_it_boots_listeners()
    {
        $events = resolve('events');
        $listeners = (new ReflectionClass($events))->getProperty('listeners')->getValue($events);

        $this->assertArrayHasKey(Event::class, $listeners);
        $this->assertContains(Listener::class, data_get($listeners, Event::class));
    }

    public function test_it_boots_subscribers()
    {
        $events = resolve('events');
        $listeners = (new ReflectionClass($events))->getProperty('listeners')->getValue($events);

        $this->assertArrayHasKey(Event::class, $listeners);
        $this->assertContains(
            Subscriber::class,
            collect(data_get($listeners, Event::class))->flatten()
        );
    }
}

class Model extends Eloquent\Model
{
}

class Observer
{
    public function creating(Model $model)
    {
    }
}

class Event
{
}

class Listener
{
}

class Subscriber
{
    public function handle(Event $event)
    {
    }

    public function subscribe($events)
    {
        return [
            Event::class => 'handle',
        ];
    }
}
