<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Support\Facades\Gate;

trait InteractsWithGateCases
{
    public function test_it_boots_policies()
    {
        $policies = Gate::policies();

        $this->assertArrayHasKey(Model::class, $policies);
        $this->assertSame(Policy::class, data_get($policies, Model::class));
    }

    public function test_it_boots_gate_definitions()
    {
        $abilities = Gate::abilities();

        $this->assertArrayHasKey('example', $abilities);
        $this->assertTrue(
            data_get($abilities, 'example')()
        );
    }
}
