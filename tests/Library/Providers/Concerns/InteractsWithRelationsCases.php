<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Database\Eloquent\Relations\Relation;

trait InteractsWithRelationsCases
{
    public function test_it_does_not_require_morph_map_by_default()
    {
        $this->assertFalse(
            Relation::requiresMorphMap()
        );
    }

    public function test_it_can_require_morph_map()
    {
        app()->register(RequireMorphMap::class);

        $this->assertTrue(
            Relation::requiresMorphMap()
        );
    }

    public function test_it_registers_morph_maps()
    {
        $this->assertSame(
            Relation::morphMap(),
            ['model' => Model::class]
        );
    }
}

class RequireMorphMap extends \Standard\Library\Providers\ServiceProvider
{
    protected bool $requireMorphMap = true;
}
