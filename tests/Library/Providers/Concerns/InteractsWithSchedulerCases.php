<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Console\Scheduling\Schedule;

trait InteractsWithSchedulerCases
{
    public function test_it_schedules_tasks()
    {
        $this->markTestSkipped(
            '`orchestra/testbench` does return the same singleton scheduler. Specifically, resolving the Schedule in a ServiceProvider and TestCase does not reference the same object in memory which prevents testing this feature currently.'
        );

        $scheduled = resolve(Schedule::class)->events();
    }
}
