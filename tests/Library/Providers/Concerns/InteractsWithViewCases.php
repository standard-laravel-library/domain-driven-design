<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Contracts\View\Factory;

trait InteractsWithViewCases
{
    public function test_it_boots_views()
    {
        $hints = resolve(Factory::class)->getFinder()->getHints();

        $this->assertArrayHasKey('one', $hints);
        $this->assertContains('path/one', data_get($hints, 'one'));
    }
}
