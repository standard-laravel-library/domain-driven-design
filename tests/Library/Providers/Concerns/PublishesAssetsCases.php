<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Support\Collection;

trait PublishesAssetsCases
{
    public function test_it_boots_publisable_assets()
    {
        tap(collect($this->getPackageProviders($this->app)), function (Collection $providers) {
            $providers->each(function ($class) {
                $this->assertTrue(
                    collect($this->app->getLoadedProviders())->get($class)
                );

                $this->assertTrue(
                    collect($class::publishableGroups())->contains('standard:group')
                );

                $this->assertEquals(
                    collect($class::$publishes)->get($class),
                    [
                        '/source/one' => '/destination/one',
                        '/source/two' => '/destination/two',
                    ]
                );
            });
        });
    }
}

trait Publishables
{
    protected function publishables(): iterable
    {
        return [
            'standard:group' => [
                '/source/one' => '/destination/one',
                '/source/two' => '/destination/two',
            ]
        ];
    }
}
