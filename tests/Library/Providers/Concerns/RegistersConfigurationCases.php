<?php

namespace Tests\Library\Providers\Concerns;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Console\ConfigCacheCommand;
use Illuminate\Foundation\Console\ConfigClearCommand;
use Tests\Library\Providers\LoadedAfterConfigurationCachedServiceProvider;

trait RegistersConfigurationCases
{
    public function test_it_registers_basic_configuration()
    {
        $this->assertSame(
            1,
            config('standard.one')
        );
    }

    public function test_it_registers_advanced_configuration()
    {
        $this->assertSame(
            2,
            config('standard.two')
        );
    }

    public function test_it_registers_overwritten_configuration()
    {
        $this->assertSame(
            3,
            config('standard.three')
        );
    }

    public function test_it_registers_merged_configuration()
    {
        $this->assertSame(
            1,
            config('standard.one')
        );

        $this->assertSame(
            2,
            config('standard.two')
        );
    }

    public function test_it_skips_configuration_registration_when_cached()
    {
        Artisan::call(ConfigCacheCommand::class);

        $this->app->register(LoadedAfterConfigurationCachedServiceProvider::class);

        $this->assertNull(
            config('standard.four')
        );

        Artisan::call(ConfigClearCommand::class);
    }
}
