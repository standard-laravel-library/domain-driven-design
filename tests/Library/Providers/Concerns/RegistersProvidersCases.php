<?php

namespace Tests\Library\Providers\Concerns;

trait RegistersProvidersCases
{
    public function test_it_registers_providers()
    {
        $this->assertSame(
            ServiceProvider::class,
            resolve(ServiceProvider::class)
        );
    }
}

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $providers = [
        ChildServiceProvider::class,
    ];

    protected function beforeRegister(): void
    {
        app()->bind(static::class, fn () => static::class);
    }

    protected function config(): array
    {
        return [
            'standard' => [
                'two' => 2,
            ]
        ];
    }
}

class ChildServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $config = [
        'standard' => [
            'three' => 3,
        ]
    ];
}
