<?php

namespace Tests\Library\Providers;

use Tests\TestCase;
use Illuminate\Support\Facades\Gate;
use Illuminate\Console\Scheduling\Schedule;

class ServiceProviderTest extends TestCase
{
    use Concerns\InitializesTraitsCases;
    use Concerns\InteractsWithConsoleCases;
    use Concerns\InteractsWithEventsCases;
    use Concerns\InteractsWithGateCases;
    use Concerns\InteractsWithSchedulerCases;
    use Concerns\InteractsWithViewCases;
    use Concerns\PublishesAssetsCases;
    use Concerns\InteractsWithRelationsCases;
    use Concerns\RegistersProvidersCases;
    use Concerns\RegistersConfigurationCases;

    public function test_before_register_hook()
    {
        $this->assertSame(
            'beforeRegister',
            resolve('beforeRegister')
        );
    }

    public function test_after_register_hook()
    {
        $this->assertSame(
            'afterRegister',
            resolve('afterRegister')
        );
    }

    public function test_before_boot_hook()
    {
        $this->assertSame(
            'beforeBoot',
            resolve('beforeBoot')
        );
    }

    public function test_after_boot_hook()
    {
        $this->assertSame(
            'afterBoot',
            resolve('afterBoot')
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
        ];
    }
}

class ServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    use Concerns\Initializer;
    use Concerns\Publishables;

    protected array $config = [
        'standard' => [
            'one' => 1,
            'three' => null
        ]
    ];

    protected array $providers = [
        Concerns\ServiceProvider::class,
    ];

    protected array $commands = [
        Concerns\Command::class,
    ];

    protected array $listen = [
        Concerns\Event::class => Concerns\Listener::class,
    ];

    protected array $subscribe = [
        Concerns\Subscriber::class,
    ];

    protected array $observers = [
        Concerns\Model::class => Concerns\Observer::class,
    ];

    protected array $policies = [
        Concerns\Model::class => Concerns\Policy::class,
    ];

    protected array $morphMap = [
        'model' => Concerns\Model::class,
    ];

    protected array $views = [
        'one' => 'path/one',
    ];

    protected function beforeRegister(): void
    {
        app()->bind('beforeRegister', fn () => 'beforeRegister');
    }

    protected function afterRegister(): void
    {
        app()->bind('afterRegister', fn () => 'afterRegister');
    }

    protected function beforeBoot(): void
    {
        app()->bind('beforeBoot', fn () => 'beforeBoot');
    }

    protected function afterBoot(): void
    {
        app()->bind('afterBoot', fn () => 'afterBoot');
    }

    protected function bootGateDefinitions(): void
    {
        Gate::define('example', fn () => true);
    }

    protected function bootScheduledTasks(Schedule $schedule): void
    {
        $schedule->call(fn () => true)->everyMinute();
    }
}

class LoadedAfterConfigurationCachedServiceProvider extends \Standard\Library\Providers\ServiceProvider
{
    protected array $config = [
        'standard' => [
            'four' => 4,
        ]
    ];
}
